/**
 * Created by OmarTeodoro on 10/11/2014.
 */

var MainIntro = {

    preload: function() {   },

    create: function() {

        this.audio = this.game.add.audio('audio', 1, false);
        this.audio.play();

        //this.background = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'background');
        //this.background.anchor.setTo(0.5, 0.5);

        this.logo = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
        this.logo.anchor.setTo(0.5, 0.5);

        //this.game.add.tween(this.background).to({ alpha: 1}, 2000, Phaser.Easing.Bounce.InOut, true);
        this.game.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);

        this.instructionsText = this.game.add.text(this.game.world.centerX, 400, 'Click anywhere to play', { font: '16px Arial', fill: '#ffffff', align: 'center'});
        this.instructionsText.alpha = 0;
        this.instructionsText.anchor.setTo(0.5, 0.5);
        this.game.add.tween(this.instructionsText).to({ alpha: 1}, 7000, Phaser.Easing.Bounce.InOut, true);
        this.game.add.tween(this.instructionsText).to({y:395}, 350, Phaser.Easing.Linear.NONE, true, 0, 1000, true);

        this.game.input.onDown.addOnce(function(){

            this.instructionsText.destroy(true);

            //this.game.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
            this.tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Linear.None, true);
            this.tween.onComplete.add(this.startMainMenu, this);

        }, this);

    },

    update: function(){   },

    startMainMenu: function() {

        this.game.state.start('main_menu');

    }

};