/**
 * Created by OmarTeodoro on 09/11/2014.
 */

var Preload = {

    preload: function() {

        this.loadingBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loadingBar');
        this.loadingBar.anchor.setTo(0.5, 0.5);
        this.load.setPreloadSprite(this.loadingBar);

        // load all of the assets
        this.game.load.spritesheet('teo', 'public/assets/teo.png', 24, 32);

        this.game.load.image('logo', 'public/assets/main_intro.jpg');
        //this.game.load.image('background', 'assets/background.jpg');

        this.game.load.audio('audio', 'public/assets/super_mario_music.mp3', true);
    },

    create: function() {
        this.tween = this.add.tween(this.loadingBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
        this.tween.onComplete.add(this.startMainIntro, this);
    },

    startMainIntro: function() {
        this.game.state.start('main_intro');
    }
};