/**
 * Created by OmarTeodoro on 10/11/2014.
 */
/**
 * Created by OmarTeodoro on 10/11/2014.
 */

var Level1 = {

    preload: function() {
        this.cursors = this.game.input.keyboard.createCursorKeys();
    },

    create: function() {

        this.game.stage.backgroundColor = '#688008';

        this.teoPlayer = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'teo');

        //this.teoPlayer.alpha = 0.5;

        this.teoPlayer.animations.add('upwalk',[0,1,2],10,true);
        this.teoPlayer.animations.add('rightwalk',[3,4,5],10,true);
        this.teoPlayer.animations.add('downwalk',[6,7,8],10,true);
        this.teoPlayer.animations.add('leftwalk',[9,10,11],10,true);

        this.teoPlayer.health = 100;
        this.teoPlayer.scale.x = 1;
        this.teoPlayer.scale.y = 1;

    },

    update: function(){

        this.movePlayer();

    },

    movePlayer: function(){

        if (this.cursors.right.isDown) {

            this.teoPlayer.body.velocity.x = 150;
            this.teoPlayer.animations.play('rightwalk');

        } else if(this.cursors.left.isDown) {

            this.teoPlayer.body.velocity.x = -150;
            this.teoPlayer.animations.play('leftwalk');

        } else if (this.cursors.up.isDown) {

            this.teoPlayer.body.velocity.y = -150;
            this.teoPlayer.animations.play('upwalk');

        } else if (this.cursors.down.isDown) {

            this.teoPlayer.body.velocity.y = 150;
            this.teoPlayer.animations.play('downwalk');

        } else {

            this.teoPlayer.body.velocity.x = 0;
            this.teoPlayer.body.velocity.y = 0;
            this.teoPlayer.animations.frame = 7;

        }

    }

};