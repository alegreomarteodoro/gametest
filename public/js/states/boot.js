/**
 * Created by OmarTeodoro on 09/11/2014.
 */

var Boot = {

    preload: function() {

        this.game.load.image('loadingBar', 'public/assets/loading.png');

    },

    create: function() {

        this.game.input.maxPointers = 1;
        this.game.stage.disableVisibilityChange = true;

        if (this.game.device.desktop) {

            //this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        } else {

            //this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            //this.game.scale.minWidth = 480;
            //this.game.scale.minHeight = 260;
            //this.game.scale.maxWidth = 1024;
            //this.game.scale.maxHeight = 768;
            //this.game.scale.forceLandscape = true;
            //this.game.scale.pageAlignHorizontally = true;
            //this.game.scale.setScreenSize(true);

        }

        this.game.state.start('preloader');

    },

    update: function() { }

};