/**
 * Created by OmarTeodoro on 10/11/2014.
 */

window.onload = function () {
    var game = new Phaser.Game(Config.width, Config.height, Phaser.AUTO, 'gameArea');
    game.state.add('boot', Boot, true);
    game.state.add('preloader', Preload);
    game.state.add('main_intro', MainIntro);
    game.state.add('main_menu', MainMenu);
    game.state.add('level1', Level1);

    game.state.start('boot');
};